package oe.java;

import oe.java.dragons.Dragon;

import java.util.ArrayList;
import java.util.Vector;

public class Trainer {
    public String name;

    private transient ArrayList<Dragon> dragons;

    public Trainer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Dragon> getDragons() {
        if( this.dragons == null ){
            this.dragons = new ArrayList<>();
        }

        return dragons;
    }

    public void addDragon(Dragon dragon){
        if( this.dragons == null ){
            this.dragons = new ArrayList<>();
        }
        dragon.setTrainer(this);
        this.dragons.add(dragon);
    }

    public void removeDragon(Dragon dragon) {
        this.dragons.remove(dragon);
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "name='" + name + '\'' +
                '}';
    }
}
