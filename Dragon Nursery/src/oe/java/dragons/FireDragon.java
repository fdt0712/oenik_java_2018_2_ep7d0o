package oe.java.dragons;

public class FireDragon extends Dragon {

    private Integer temperature;

    public FireDragon() {
        super();
        setBreathType(BreathType.FIRE);
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }
}
