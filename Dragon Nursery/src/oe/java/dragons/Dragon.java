package oe.java.dragons;

import oe.java.Trainer;

import java.util.Date;

public class Dragon {
    private Date birthDate;
    private Integer age;
    private String color;
    private Integer size;
    private Boolean adult;
    private String name;
    private BreathType breathType;
    private Trainer trainer;

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Boolean getAdult() {
        return adult;
    }

    public void setAdult(Boolean adult) {
        this.adult = adult;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BreathType getBreathType() {
        return breathType;
    }

    protected void setBreathType(BreathType breathType) {
        this.breathType = breathType;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    @Override
    public String toString() {
        return "Dragon{" +
                "name='" + name + '\'' +
                '}';
    }
}