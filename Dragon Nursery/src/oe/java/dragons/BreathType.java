package oe.java.dragons;

public enum  BreathType {
    FIRE,
    FROST,
    ACID
}
