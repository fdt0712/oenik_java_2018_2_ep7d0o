package oe.java.dragons;

public class IceDragon extends Dragon {

    private Boolean IceShield;

    private Integer temperature;

    public IceDragon() {
        super();
        setBreathType(BreathType.FROST);
    }

    public Boolean hasIceShield() {
        return IceShield;
    }

    public void setIceShield(Boolean iceShield) {
        IceShield = iceShield;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }
}
