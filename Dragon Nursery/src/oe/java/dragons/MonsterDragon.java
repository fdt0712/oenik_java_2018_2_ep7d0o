package oe.java.dragons;

public class MonsterDragon extends Dragon {

    public MonsterDragon() {
        super();
        setBreathType(BreathType.ACID);
    }
}
