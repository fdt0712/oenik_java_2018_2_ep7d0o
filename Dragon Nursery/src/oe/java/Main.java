package oe.java;

import oe.java.dragons.Dragon;
import oe.java.dragons.FireDragon;
import oe.java.dragons.IceDragon;
import oe.java.dragons.MonsterDragon;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class Main {

    public static void main(String[] args) {

        // DragonLogic.createJSON();

        List<Dragon> dragons = DragonLogic.loadJSON();
        if( dragons == null ){
            return;
        }

        writeList(dragons);

        writeGroupByDra(dragons);

        inputDragon(dragons);

    }

    public static void writeList(List<Dragon> dragons){

        for (Iterator<Dragon> i = dragons.iterator(); i.hasNext();) {
            Dragon item = i.next();
            System.out.println( item +  (( item.getTrainer() != null )  ? ", Trainer is " + item.getTrainer() : "" ) );

        }

    }

    public static void writeGroupByDra(List<Dragon> dragons){

        ArrayList<Dragon> _dragons = new ArrayList<>();
        for (Iterator<Dragon> i = dragons.iterator(); i.hasNext();) {
            Dragon item = i.next();
            if( item.getTrainer() != null ) {
                _dragons.add(item);
            }
        }

        Map<Trainer, List<Dragon>> trainers = DragonLogic.getGroupedByTrainer(_dragons);
        trainers
            .forEach((trainer, p) -> {
                if( trainer == null ){
                    return;
                }
                System.out.println(trainer);

                for (int i = 0; i < trainer.getDragons().size(); i++) {
                    int k = i;
                    System.out.println("-" + trainer.getDragons().get(k));
                }
            });
    }

    public static void inputDragon(List<Dragon> dragons){

        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Select a dragon type ( f for FireDragon, i for IceDragon, m for MonsterDragon):");
            String choice = br.readLine();
            Dragon dragon;
            switch (choice){
                case "f":
                    dragon = new FireDragon();
                    break;
                case "i":
                    dragon = new IceDragon();
                    break;
                case "m":
                    dragon = new MonsterDragon();
                    break;
                default:
                    dragon = new Dragon();
                    break;
            }

            System.out.print("Enter name:");
            dragon.setName(br.readLine());

            System.out.print("Enter age:");
            dragon.setAge(Integer.parseInt(br.readLine()));

            dragon.setBirthDate(new Date());

            System.out.print("Enter size:");
            dragon.setSize(Integer.parseInt(br.readLine()));
            System.out.print("Enter color:");
            dragon.setColor(br.readLine());
            dragon.setAdult(true);


            if( !dragons.contains(dragon) ){
                dragons.add(dragon);
                DragonLogic.saveToJson(dragons);
            }
        }catch(Exception nfe){
            System.err.println("Invalid Format!");
        }

    }
}
