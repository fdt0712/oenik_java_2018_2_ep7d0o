package oe.java;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import oe.java.dragons.Dragon;
import oe.java.dragons.FireDragon;
import oe.java.dragons.IceDragon;
import oe.java.dragons.MonsterDragon;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;

public class DragonLogic {

    private static String FILE_NAME = "Trainers.json";

    public static List<Dragon> loadJSON(){

        List<Dragon> dragons;

        try (Reader reader = new FileReader(FILE_NAME)) {
            Type listType = new TypeToken<ArrayList<Dragon>>(){}.getType();
            dragons = new Gson().fromJson(reader, listType);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }

        for (Iterator<Dragon> i = dragons.iterator(); i.hasNext();) {
            Dragon item = i.next();
            if( item.getTrainer() != null ) {
                item.getTrainer().addDragon(item);
            }
        }

        return dragons;
    }

    public static void createJSON(){
        Trainer trainer1 = new Trainer("Trainer#1");

        Dragon fireDragon = new FireDragon();
        fireDragon.setAdult(false);
        fireDragon.setAge(20);
        fireDragon.setBirthDate(new Date());
        fireDragon.setColor("#2f2f2f");
        fireDragon.setName("Firedragon");
        fireDragon.setSize(20);
        ((FireDragon) fireDragon).setTemperature(30);
        Dragon iceDragon = new IceDragon();
        iceDragon.setAdult(true);
        iceDragon.setAge(10);
        iceDragon.setBirthDate(new Date());
        iceDragon.setColor("#21df34");
        iceDragon.setName("iceDragon");
        iceDragon.setSize(2);
        ((IceDragon) iceDragon).setIceShield(true);
        trainer1.addDragon(fireDragon);
        trainer1.addDragon(iceDragon);

        Trainer trainer2 = new Trainer("Trainer#2");
        Dragon monsterDragon = new MonsterDragon();
        monsterDragon.setAdult(true);
        monsterDragon.setAge(14);
        monsterDragon.setBirthDate(new Date());
        monsterDragon.setColor("#21df34");
        monsterDragon.setName("A good monster dragon");
        monsterDragon.setSize(22);
        trainer2.addDragon(monsterDragon);

        Trainer trainer3 = new Trainer("Trainer#3");
        Dragon monsterDragon2 = new MonsterDragon();
        monsterDragon2.setAdult(true);
        monsterDragon2.setAge(1);
        monsterDragon2.setBirthDate(new Date());
        monsterDragon2.setColor("#21df34");
        monsterDragon2.setName("A bad monster dragon");
        monsterDragon2.setSize(16);
        trainer3.addDragon(monsterDragon2);
        Dragon iceDragon2 = new IceDragon();
        iceDragon2.setAdult(false);
        iceDragon2.setAge(40);
        iceDragon2.setBirthDate(new Date());
        iceDragon2.setColor("#21df34");
        iceDragon2.setName("An ice dragon");
        iceDragon2.setSize(4);
        trainer3.addDragon(iceDragon2);

        Iterable<Dragon> dragons = new ArrayList<Dragon>();
        ((ArrayList<Dragon>) dragons).add(fireDragon);
        ((ArrayList<Dragon>) dragons).add(iceDragon);
        ((ArrayList<Dragon>) dragons).add(monsterDragon);
        ((ArrayList<Dragon>) dragons).add(monsterDragon2);
        ((ArrayList<Dragon>) dragons).add(iceDragon2);
        // Since one dragon can have only one trainer,
        // its easier to save the trainers
        saveToJson(dragons);
    }

    /**
     *
     * @param dragons
     */
    public static void saveToJson(Iterable<Dragon> dragons){
        try (Writer writer = new FileWriter(FILE_NAME)) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(dragons, writer);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public static Map<Trainer, List<Dragon>> getGroupedByTrainer(List<Dragon> dragons){
        return dragons.stream()
                .collect(groupingBy(Dragon::getTrainer));
    }
}
