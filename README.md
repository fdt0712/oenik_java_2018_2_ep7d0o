12.) CREATE A JAVA PROGRAM THAT IMPLEMENTS THE FOLLOWING REQUIREMENTS:

The program implements a solution for a Dragon Nursery.   
Dragons have the following attributes: birthdate, age, color, size, adult(boolean), name, breathType (fire/frost/acid etc.)   
It is possible to have different species of dragons (atleast 3) that have extra attributes besides of what a dragon has generally.  
There are also at least 2 instances of Dragon Trainers, who have dragons. One Dragon can only belong to one Trainer. 
  
The program should operate in the following way:  
 1.) The program reads in Dragons from the hard disk.  
 2.) The program then lists all Dragons that it read in, grouping them by their trainers whose name starts the list.  
 3.) The program prompts the user to add a new dragon to the list of dragons.   
 4.) The program saves this new Dragon into memory if it is not identical to any other dragons already existing.  
 5.) The program saves all Dragons to the hard disk.  
